package tb.sockets.server;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParseException;
import java.text.ParsePosition;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.text.MaskFormatter;
import java.awt.Color;
import java.util.ArrayList;

public class MainFrameServer extends JFrame {

	private JPanel contentPane;
	static ServerSocket sServer;
	static Socket sock;
	static Konsola konsolaSerwer;
	static String messageToSend;
	static String messageFromServer;
	static JTextArea showText;
	static PrintStream ps;
	JFrame frame;
	boolean newMessage;
	String host;
	int port;
	//Socket sock;
	static boolean sendMsg = false;
	DataOutputStream so;
	InputStream inputFromServer;
	static String messageFromServerK;
	boolean newMessageK = false;
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrameServer mainframe = new MainFrameServer();
					mainframe.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		Thread runServer = new Thread(new Runnable() {
			@Override
			public void run() {
				 KonsolaServer();
			}
		});
		runServer.start();
	}

	public MainFrameServer() {

		messageToSend = "seima";
		UI();
	}

	public static void setText(String text)
	{
		showText.append("\n" + text );
	}


	public void UI()
	{

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 650, 500);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		//JButton btnConnect = new JButton("Connect");
		//btnConnect.setBounds(10, 70, 75, 23);
		//contentPane.add(btnConnect);



		JButton btnSend = new JButton("Wyslij");
		btnSend.setBounds(10,100, 75,23);
		contentPane.add(btnSend);

		JFormattedTextField textToSend = new JFormattedTextField();
		textToSend.setBounds(200,50,200,100);
		contentPane.add(textToSend);

		showText = new JTextArea();
		showText.setBounds(10,200,500,200);
		contentPane.add(showText);
		//	setShowText();
		btnSend.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				messageToSend = textToSend.getText();
				sendMsg =true;

				//KonsolaClient(messageToSend);
				KonsolaServer();
			}
		});

		//btnConnect.addActionListener(new ActionListener()
	//	{
	//		@Override
	//		public void actionPerformed(ActionEvent e)
		//	{
			//	host = hostText.getText();
			//	port = Integer.parseInt(portText.getText());
			//	KonsolaClient(host, port, "hello server!");
		//	}
		}


//	}
	public static void KonsolaServer() {
		ArrayList clientOutputStreams;
		String messageFromClient;
		PrintWriter writer;
		InputStreamReader ir;
		BufferedReader br;

		try{

			 sServer = new ServerSocket(6666);
			do {
				 sock = sServer.accept();
				if(sock.isConnected())
					System.out.println("Polaczano z serwerem!");

				ir = new InputStreamReader(sock.getInputStream());
				br = new BufferedReader(ir);
				messageFromClient = br.readLine();
				if(sendMsg == true) {
					ps = new PrintStream((sock.getOutputStream()));
					ps.println("Server: " + messageToSend);
					setText("Server: " + messageToSend );
				}
				sendMsg=false;
				if (messageFromClient != null) {
					setText("Klient: " + messageFromClient);
					ps = new PrintStream((sock.getOutputStream()));
					ps.println(sock.getInetAddress().getHostName() + ": " + messageFromClient);
				}
				}while(!messageFromClient.equals("end"));
			System.out.println("Zakonczono polaczenie");
		} catch(Exception e)
		{

		}
	}

	public void KonsolaClient(String message) {
		Thread runK = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					sock = new Socket(host, port);	//server ip and port
					PrintStream ps = new PrintStream(sock.getOutputStream());
					ps.println(message);

					InputStreamReader ir = new InputStreamReader(sock.getInputStream());
					BufferedReader br = new BufferedReader(ir);
					messageFromServerK = br.readLine();
					messageFromServerK = messageFromServerK + "\n" ;
					setText(messageFromServerK );

					System.out.println(messageFromServerK);

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		runK.start();


	}

}
