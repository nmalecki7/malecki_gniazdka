package tb.sockets.client;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.io.*;
import java.net.Socket;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParseException;
import java.text.ParsePosition;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.text.MaskFormatter;
import java.awt.Color;

public class MainFrame extends JFrame {

	private JPanel contentPane;
	tb.sockets.server.Konsola konsolaSerwer;
	String messageToSend;
	static String messageFromServer;
	static JTextArea showText;
	JFrame frame;
	boolean newMessage;
    String host;
    int port;
	Socket sock;
	DataOutputStream so;
	InputStream inputFromServer;
	static String messageFromServerK;
	boolean newMessageK = false;
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame mainframe = new MainFrame();
					mainframe.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MainFrame() {

        messageToSend = "";
        UI();


	}

	public static void setText(String text)
	{
		showText.append( text );
	}


	public void UI()
	{

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 650, 500);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblHost = new JLabel("Host:");
		lblHost.setBounds(10, 14, 26, 14);
		contentPane.add(lblHost);

		JFormattedTextField hostText;
		hostText = new JFormattedTextField();
		hostText.setBounds(43, 11, 90, 20);
		contentPane.add(hostText);


		JButton btnConnect = new JButton("Connect");
		btnConnect.setBounds(10, 70, 75, 23);
		contentPane.add(btnConnect);

		JFormattedTextField portText = new JFormattedTextField();
		portText.setBounds(43, 39, 90, 20);
		contentPane.add(portText);

		JLabel lblPort = new JLabel("Port:");
		lblPort.setBounds(10, 42, 26, 14);
		contentPane.add(lblPort);

		JButton btnSend = new JButton("Wyslij");
		btnSend.setBounds(10,100, 75,23);
		contentPane.add(btnSend);

		JFormattedTextField textToSend = new JFormattedTextField();
		textToSend.setBounds(200,50,200,100);
		contentPane.add(textToSend);

		showText = new JTextArea();
		showText.setBounds(10,200,500,200);
		contentPane.add(showText);
	//	setShowText();
		btnSend.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				messageToSend = textToSend.getText();
				 Konsola(host, port, messageToSend);
			}
		});

		btnConnect.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				host = hostText.getText();
				port = Integer.parseInt(portText.getText());

				Konsola(host, port, "hello server!");

			}
		});


	}
	public void Konsola(String host, int port, String message) {
		Thread runK = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					sock = new Socket(host, port);	//server ip and port
					PrintStream ps = new PrintStream(sock.getOutputStream());
					ps.println(message);

					InputStreamReader ir = new InputStreamReader(sock.getInputStream());
					BufferedReader br = new BufferedReader(ir);
					messageFromServerK = br.readLine();
					messageFromServerK = messageFromServerK + "\n" ;
					setText(messageFromServerK );

					System.out.println(messageFromServerK);

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		runK.start();


	}




}
